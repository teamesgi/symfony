<?php

namespace AppBundle\Controller;

use \Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AppController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        if($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $url = $this->get('router')->generate('dashboard');
        } else {
            $url = $this->get('router')->generate('fos_user_security_login');
        }

        return $this->redirect($url);
    }
}