<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Sprint;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 */
class SprintController extends Controller
{
    /**
     * @Route("/sprints/{version}", name="sprint_show")
     * @Method("GET")
     * @Template(":admin/sprint:sprint_show.html.twig")
     */
    public function sprintShowAction(Sprint $sprint)
    {
        return [
            'sprint' => $sprint
        ];
    }
}