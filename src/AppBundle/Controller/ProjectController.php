<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use AppBundle\Entity\Task;
use AppBundle\Entity\Project;


/**
 * Project controller.
 *
 * @Route("/admin")
 */
class ProjectController extends Controller
{
    /**
     * @Route("/projects", name="projects")
     * @Method("GET")
     * @Template(":admin/project:projects.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $projects = $em->getRepository('AppBundle:Project')->findAll();

        return [
            'projects' => $projects
        ];
    }

    /**
     * Save form project
     *
     * @Route("/project-form", name="project_form", options={"expose"=true})
     * @Method("GET")
     * @Template(":admin:form.html.twig")
     */
    public function formAction(Request $request)
    {
        $project = new Project();
        $form = $this->createForm('AppBundle\Form\ProjectType', $project);
        $form->handleRequest($request);

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/projects/{code}/kanban", name="project_kanban")
     * @Template(":admin/project:kaban.html.twig")
     */
    public function kabanAction(Project $project)
    {
		$tasks = [];

		$em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle\Entity\Task');

    	$tasks['todo']		= $repository->findBy([
            'project' => $project,
            'status' => 'open'
        ]);
    	$tasks['progress'] 	= $repository->findBy([
            'project' => $project,
            'status' => 'in progress'
        ]);
    	$tasks['done']		= $repository->findBy([
            'project' => $project,
            'status' => 'closed'
        ]);

		return [
            'project' => $project,
            'tasks' => $tasks
        ];
    }

    /**
     * @Route("/projects/{code}/sprints", name="project_sprints")
     * @Template(":admin/project:sprints.html.twig")
     */
    public function sprintsAction(Project $project)
    {
        $em = $this->getDoctrine()->getManager();
        $sprints = $em->getRepository("AppBundle:Sprint")->findByProject($project);

        return [
            'project' => $project,
            'sprints' => $sprints
        ];
    }

    /**
     * @Route("/projects/{code}/tasks", name="project_tasks")
     * @Template(":admin/project:tasks.html.twig")
     */
    public function tasksAction(Project $project)
    {
        $em = $this->getDoctrine()->getManager();
        $tasks = $em->getRepository("AppBundle:Task")->findByProject($project);

        return [
            'project' => $project,
            'tasks' => $tasks
        ];
    }

    /**
     * @Route("/task/edit/{id}", name="task_edit", options={"expose"=true})
     * @Method({"GET", "POST"})
     */
    public function taskEditAction(Request $request, Task $task)
    {

        $em = $this->getDoctrine()->getManager();
        
        $status = $request->get('status');

        $task->setStatus($status);
        $em->flush();

		return new Response(json_encode(['Moved' => 'Moved']));
    }
}
