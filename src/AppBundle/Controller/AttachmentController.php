<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Attachment;
use AppBundle\Form\AttachmentType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @Route("/admin")
 */
class AttachmentController extends Controller
{
    /**
     * @Route("/upload", name="attachment")
     * @Template("::attachment.html.twig")
     */
    public function uploadAction(Request $request)
    {
        $attachment = new Attachment();
        $form = $this->createForm(new AttachmentType(), $attachment);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $file = $form['file']->getData();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $attachmentDir = $this->container->getParameter('kernel.root_dir').'/../web/uploads/attachments';
            $file->move($attachmentDir, $fileName);
            $attachment->setFile($fileName);

            $em = $this->getDoctrine()->getManager();
            $em->persist($attachment);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Your attachement has been saved!');

            return $this->redirect($this->generateUrl('attachment'));
        }

        return ['form' => $form->createView()];
    }
}