<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Task;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 */
class TaskController extends Controller
{
    /**
     * @Route("/tasks", name="tasks")
     * @Template(":admin/task:tasks.html.twig")
     */
    public function indexAction(Request $request)
    {
        return [];
    }

    /**
     * @Route("/tasks/{code}", name="task_show")
     * @Method("GET")
     * @Template(":admin/task:task_show.html.twig")
     */
    public function taskShowAction(Task $task)
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('UserBundle:User')->findAll();

        return [
            'task' => $task,
            'users'=> $users
        ];
    }

    /**
     * @Route("/tasks/assign", name="task_assign")404 Not Found - NotFoundHttpException

     */
    public function assignTaskAction(Request $request)
    {
        $redirectUrl = $this->get('app.task.manager')->changeStatusAndRedirect($request);

        return $this->redirect($redirectUrl);
    }

    /**
     * @Route("/tasks/change-status", name="task_status")
     */
    public function changeTaskAction(Request $request)
    {
        $redirectUrl = $this->get('app.task.manager')->changeStatusAndRedirect($request);

        return $this->redirect($redirectUrl);
    }
}