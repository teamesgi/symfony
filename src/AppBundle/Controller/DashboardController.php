<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 */
class DashboardController extends Controller
{
    /**
     * @Route("/dashboard", name="dashboard")
     * @Template("admin/dashboard.html.twig")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $projects = $em->getRepository("AppBundle:Project")->findByMembers(array($user));
        $tasks = $em->getRepository("AppBundle:Task")->findByAssignee($user);

        return [
            'projects' => $projects,
            'tasks' => $tasks
        ];
    }
}
