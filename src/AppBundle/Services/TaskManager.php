<?php

namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\Request;

class TaskManager
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Router$taskUrl
     */
    private $router;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, Router $router)
    {
        $this->em = $em;
        $this->router = $router;
    }

    /**
     * @param Request $request
     * @return string
     */
    public function changeStatusAndRedirect(Request $request)
    {
        $taskCode = $request->get('taskCode');
        $statusTask = $request->get('status');

        $task = $this->em->getRepository('AppBundle:Task')->findOneByCode($taskCode);

        $task->setStatus($statusTask);
        $this->em->flush();

        return $this->router->generate('task_show', ['code' => $taskCode]);
    }

    /**
     * @param Request $request
     * @return string
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function assignTaskAndRedirect(Request $request)
    {
        $taskCode = $request->get('taskCode');
        $userId = $request->get('userId');

        $task = $this->em->getRepository('AppBundle:Task')->findOneByCode($taskCode);
        $user = $this->em->find('UserBundle:User', $userId);

        $task->setAssignee($user);
        $this->em->flush();

       return $this->router->generate('task_show', ['code' => $taskCode]);
    }
}