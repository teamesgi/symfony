<?php

namespace AppBundle\Twig;

use AppBundle\Entity\Task;
use Mapping\Fixture\Xml\Status;

class AppExtension extends \Twig_Extension
{

    public function getGlobals()
    {
        $task = new \ReflectionClass(Task::class);
        $taskConstants = $task->getConstants();

        return array(
            'Task' => $taskConstants
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app.extension';
    }
}