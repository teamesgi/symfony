<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

class AssetsComponentsInstallCommand extends ContainerAwareCommand
{
    /**
     * @var string
     */
    private $rootDir;

    /**
     * @var string
     */
    private $componentsDir;

    /**
     * @var string
     */
    private $webDir;

    protected function configure()
    {
        $this
            ->setName('assets:components:install')
            ->setDescription('Installs bower components under a public web directory');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->rootDir = str_replace('/app', '', $this->getContainer()->get('kernel')->getRootDir());
        $this->componentsDir = 'app/Resources/components';
        $this->webDir = 'web';
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Installing bower components as <comment>hard copies</comment>.');

        $bowerComponents = $this->getContainer()->getParameter('app.bower_components');

        $fs = $this->getContainer()->get('filesystem');
        $finder = new Finder();

        foreach ($bowerComponents as $component) {
            if (preg_match('/(.+)\/\*/', $component['source'], $folderPathMatches)) {
                $folderPath = $this->componentsDir . DIRECTORY_SEPARATOR . $folderPathMatches[1];
                $files = $finder->files()->in($folderPath);
                foreach ($files as $file) {
                    $folderTarget = str_replace('/*', '', $component['target'], $folderTargetIsValid);
                    if ($folderTargetIsValid !== 1) {
                        throw new Exception('The \'target\' in the config argument is invalid !');
                    }
                    $targetFile = $this->webDir . DIRECTORY_SEPARATOR . $folderTarget . DIRECTORY_SEPARATOR . $file->getRelativePathname();
                    $fs->copy($file->getRealpath(), $this->rootDir . DIRECTORY_SEPARATOR . $targetFile, true);
                    $output->writeln('Installing bower components for <comment>' . $folderPath . DIRECTORY_SEPARATOR . $file->getRelativePathname() . '</comment> into <comment>' . $targetFile . '</comment>');
                }
            } else {
                $fileSourceRelativePath = $this->componentsDir . DIRECTORY_SEPARATOR . $component['source'];
                $fileTargetRelativePath = $this->webDir . DIRECTORY_SEPARATOR . $component['target'];
                $fs->copy($this->rootDir . DIRECTORY_SEPARATOR . $fileSourceRelativePath, $this->rootDir . DIRECTORY_SEPARATOR . $fileTargetRelativePath, true);
                $output->writeln('Installing bower components for <comment>' . $fileSourceRelativePath . '</comment> into <comment>' . $fileTargetRelativePath . '</comment>');
            }
        }
    }

}