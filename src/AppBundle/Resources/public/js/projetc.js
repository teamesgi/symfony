$(function() {

    $('#tableProject').DataTable({
        "language": {
            "lengthMenu": "Afficher _MENU_ lignes par pages",
            "zeroRecords": "Aucun résultat",
            "info": "Afficher page _PAGE_ sur _PAGES_",
            "infoEmpty": "Aucun résultat",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "search": "Rechercher"
        }
    });

    $(document).on('dblclick', '#tableProject tr', function() {
        var code = $(this).find('td').eq(2).text(),
            showUrl = Routing.generate('app_projects_show', {code:code});
        document.location.href = showUrl;
    });

    $(document).on('click', '.project-modal-load', function(event) {
        event.preventDefault();
        var url = $(this).attr('href');
        $.get(url, function (data) {
            $('#projectModal .modal-content').html(data);
            $('#projectModal').modal();
        });
    });

    $(document).on('click', '.btn-delete-project', function() {
        var line = $(this).parents('tr'),
            deleteUrl = $(this).attr('data-url');
        $.ajax({method: 'DELETE', url: deleteUrl, success: function() {
            line.fadeOut();}
        });
    });

});