imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }
    - { resource: api.yml }
    - { resource: "@UserBundle/Resources/config/services.yml" }

# Put parameters here that don't need to change on each machine where the app is deployed
# http://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: fr

framework:
    #esi:             ~
    translator:      { fallbacks: ["%locale%"] }
    secret:          "%secret%"
    router:
        resource: "%kernel.root_dir%/config/routing.yml"
        strict_requirements: ~
    form:            ~
    csrf_protection: ~
    validation:      { enable_annotations: true }
    #serializer:      { enable_annotations: true }
    templating:
        engines: ['twig']
    default_locale:  "%locale%"
    trusted_hosts:   ~
    trusted_proxies: ~
    session:
        # handler_id set to null will use default session handler from php.ini
        handler_id:  ~
    fragments:       ~
    http_method_override: true

# Twig Configuration
twig:
    debug:            "%kernel.debug%"
    strict_variables: "%kernel.debug%"
    form_themes:
            - "bootstrap_3_layout.html.twig"

# Assetic Configuration (used for managing web assets: CSS, JavaScript, Sass, etc.)
assetic:
    debug:          "%kernel.debug%"
    use_controller: false
    bundles:
      - AppBundle
      - UserBundle
      - FOSUserBundle
    filters:
        cssrewrite: ~
        jsqueeze: ~
        scssphp:
            # the formatter must be the FQCN (don't use the 'compressed' value)
            formatter: "Leafo\\ScssPhp\\Formatter\\Compressed"

# Doctrine Configuration
doctrine:
    dbal:
        driver:   pdo_mysql
        host:     "%database_host%"
        port:     "%database_port%"
        dbname:   "%database_name%"
        user:     "%database_user%"
        password: "%database_password%"
        charset:  UTF8
        # if using pdo_sqlite as your database driver:
        #   1. add the path in parameters.yml
        #     e.g. database_path: "%kernel.root_dir%/data/data.db3"
        #   2. Uncomment database_path in parameters.yml.dist
        #   3. Uncomment next line:
        #     path:     "%database_path%"

    orm:
        auto_generate_proxy_classes: "%kernel.debug%"
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true

# Swiftmailer Configuration
swiftmailer:
    transport: "%mailer_transport%"
    host:      "%mailer_host%"
    username:  "%mailer_user%"
    password:  "%mailer_password%"
    spool:     { type: memory }

# FOS User
fos_user:
    db_driver: orm # other valid values are 'mongodb', 'couchdb' and 'propel'
    firewall_name: main
    user_class: UserBundle\Entity\User

# Stof Doctrine
stof_doctrine_extensions:
    orm:
        default:
            tree: true
            sluggable: true
            timestampable: true

# Nelmio Cors Configuration
nelmio_cors:
    defaults:
        allow_credentials: false
        allow_origin: []
        allow_headers: []
        allow_methods: []
        expose_headers: []
        max_age: 0
        hosts: []
        origin_regex: false
    paths:
        '^/api/':
            allow_origin: ['*']
            allow_headers: ['X-Custom-Auth']
            allow_methods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE']
            max_age: 3600
        '^/':
            origin_regex: true
            allow_origin: ['^http://localhost:[0-9]+']
            allow_headers: ['X-Custom-Auth']
            allow_methods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE']
            max_age: 3600
            hosts: ['^api\.']

# FOS Rest Configuration
fos_rest:
    param_fetcher_listener: true
    body_listener: true
    routing_loader:
        include_format: false
    format_listener:
        rules:
            - { path: '^/api/doc', priorities: ['text/html'], fallback_format: html, prefer_extension: true }
            - { path: '^/api', priorities: ['json', 'xml'], fallback_format: ~, prefer_extension: false }
            - { path: '^/', stop: true }
    view:
        view_response_listener: true
        formats:
            json: true
            xml: true
    exception:
        enabled: true
        exception_controller: 'FOS\RestBundle\Controller\ExceptionController::showAction'

# Bower
app:
    bower_components:
        - {source: font-awesome/fonts/*, target: fonts/font-awesome/*}
        - {source: bootstrap/dist/fonts/*, target: fonts/glyphicons/*}